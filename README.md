## Visual 2D First Aid Guide

An android app that has first aid guide with visual tutorials. The android application will provide a step by step way of doing basic scenarios that would need First-Aid response such as bleeding, burns, choking, shock and unconsciousness. Also, this application will provides Q & A to help the users understand the procedures of first aid.
