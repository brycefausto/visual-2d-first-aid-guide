﻿using UnityEngine;
using System.Collections;

public class RescueBreath : MonoBehaviour {

	private Animator anim;

	public bool animating, holding;
	public UnconsciousScript script;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseDown () {
		if (!script.BreathMaxed ()) {
			holding = true;
			InvokeRepeating ("Trigger", 0, 1.2f);
		}
	}

	void OnMouseUp () {
		holding = false;
	}

	public void Trigger () {
		if (!animating && holding && !script.BreathMaxed () && gameObject.activeSelf) {
			animating = true;
			anim.SetTrigger ("Trigger");
			Sounds.Play (Sounds.maskBreath);
		}
		if (!holding || script.BreathMaxed ())
			CancelInvoke ("Trigger");
	}

	public void Finish () {
		animating = false;
		script.Breath ();
	}

}
