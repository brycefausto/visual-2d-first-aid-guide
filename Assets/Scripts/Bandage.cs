﻿using UnityEngine;
using System.Collections;

public class Bandage : MonoBehaviour {

	private float mousePosY;
	private Animator anim;

	public BleedingScript script;
	public GameObject[] bandages;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown () {
		mousePosY = Input.mousePosition.y;
	}

	void OnMouseUp () {
		float diff = mousePosY - Input.mousePosition.y;
		if (diff > 50) {
			DragDown ();
		}
	}

	public void DragDown () {
		anim.enabled = true;
	}

	public void ApplyBandage(int i) {
		bandages [i].SetActive (true);
		Sounds.Play (Sounds.clothFlip);
	}

	public void TurnBandage(int i) {
		if (i == 1) {
			transform.SetAsFirstSibling ();
		} else {
			transform.SetSiblingIndex (1);
			PauseAnimation ();
		}
	}

	public void PauseAnimation() {
		anim.enabled = false;
	}

	public void Finish() {
		script.FinishBandage ();
	}

}
