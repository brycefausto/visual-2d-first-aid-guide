﻿using UnityEngine;
using System.Collections;

public class ShockSelectable : Selectable {

	public ShockScript script;

	public override void OnSelect() {
		switch (gameObject.name) {
		case "Lay Down":
			script.LayDown (this);
			break;
		case "Raise Legs":
			script.RaiseLegs (this);
			break;
		}
	}

}
