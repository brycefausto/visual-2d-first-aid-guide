﻿using UnityEngine;
using System.Collections;

public class ChokingSelectable : Selectable {

	public ChokingScript script;

	public override void OnSelect() {
		switch (gameObject.name) {
		case "Back Hit":
			script.SwitchPanel (1);
			break;
		case "Abdominal Thrust":
			script.SwitchPanel (2);
			break;
		}
	}

	public override void OnDeselect() {
		script.SwitchPanel (0);
	}

}
