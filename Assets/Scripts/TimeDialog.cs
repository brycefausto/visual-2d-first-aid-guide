﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeDialog : MonoBehaviour {

	public BurnsScript script;
	public Text minutes;
	public Slider slider;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Slide () {
		float value = slider.value;
		string minText = value + " Minute";
		if (value > 1)
			minText += "s";
		minutes.text = minText;
	}

	public void OK() {
		script.maxMins = slider.value;
		script.RunFaucet ();
		gameObject.SetActive (false);
	}

	public void Close() {
		gameObject.SetActive (false);
	}

}
