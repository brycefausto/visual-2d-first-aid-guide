﻿using UnityEngine;
using System.Collections;

public class ShockDraggable : Draggable {

	public ShockScript script;

	public override void OnPutTarget () {
		switch (gameObject.name) {
		case "Bandage":
			script.ApplyBandage (gameObject);
			break;
		case "Blanket":
			script.ApplyBlanket (gameObject);
			break;
		}
	}

	public override void OnHoldOff () {
		switch (gameObject.name) {
		case "Bandage":
			script.ShowError ();
			break;
		case "Blanket":
			script.ShowError ();
			break;
		}
	}
}
