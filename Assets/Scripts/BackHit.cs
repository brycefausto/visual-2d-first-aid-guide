﻿using UnityEngine;
using System.Collections;

public class BackHit : MonoBehaviour {

	private Animator anim;

	public bool animating, holding;
	public ChokingScript script;
	public BackHit backHit;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	//For Back Area
	void OnMouseDown () {
		backHit.OnMouseDown1 ();
	}

	void OnMouseUp () {
		backHit.holding = false;
	}

	//For Back Hit Panel
	public void OnMouseDown1 () {
		if (script.numBackHit < 5) {
			holding = true;
			InvokeRepeating ("Trigger", 0, 0.4f);
		}
	}

	public void Trigger() {
		if (!animating && holding && script.numBackHit < 5) {
			animating = true;
			anim.SetTrigger ("Trigger");
			Sounds.Play (Sounds.handHit);
		}
		if (!holding || script.numBackHit == 5)
			CancelInvoke ("Trigger");
	}

	public void Finish () {
		animating = false;
		script.IncreaseBackHit ();
	}

}
