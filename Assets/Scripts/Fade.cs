﻿using UnityEngine;
using System.Collections;

public class Fade : MonoBehaviour {

	private static float alpha = 1.0f; 

	Texture2D texture;
	float fadeSpeed = 0.4f;
	int drawDepth = -1000;

	private int fadeDir = -1;

	// Use this for initialization
	void Start () {
		texture = new Texture2D (1,1);
		texture.SetPixel (0, 0, Color.black);
		texture.Apply ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI (){
		if (alpha > 0) {
			alpha += fadeDir * fadeSpeed * Time.deltaTime;
			alpha = Mathf.Clamp01 (alpha);
			GUI.color = new Color (GUI.color.r, GUI.color.g, GUI.color.b, alpha);
			GUI.depth = drawDepth; 
			GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), texture);
		}
	}

	public static void StartFade () {
		alpha = 1.0f;
	}

}
