﻿using UnityEngine;
using System.Collections;

public class Loader : MonoBehaviour {

	public static Loader instance;

	public string sceneName;
	private Transform circle;

	// Use this for initialization
	void Start () {
		circle = transform.Find ("Circle");
		InvokeRepeating ("RotateCircle", 0, 0.05f);
		Invoke ("Load", 1);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void RotateCircle () {
		circle.Rotate (0, 0, circle.localRotation.z - 15);
	}

	private void Load () {
		AutoFade.LoadScene (sceneName, 0.3f);
	}

	public static void LoadScene (string sceneName) {
		Transform canvas = GameObject.Find ("Canvas").transform;
		Transform sceneLoader = Instantiate (Resources.Load<Transform> ("Prefabs/SceneLoader"));
		instance = sceneLoader.GetComponent<Loader> ();
		instance.sceneName = sceneName;
		sceneLoader.SetParent (canvas, false);
	}
}
