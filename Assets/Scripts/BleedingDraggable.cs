﻿using UnityEngine;
using System.Collections;

public class BleedingDraggable : Draggable {

	public BleedingScript script;

	public override void OnPutTarget () {
		switch (gameObject.name) {
		case "Faucet":
			script.RunFaucet (gameObject);
			break;
		case "Plaster":
			script.ApplyPlaster (gameObject);
			break;
		case "Towel":
			script.ApplyPressure (gameObject);
			break;
		case "Bandage":
			script.ApplyBandage (gameObject);
			break;
		}
	}

	public override void OnDragTarget () {
		switch (gameObject.name) {
		case "Soap":
			script.ApplySoap (gameObject);
			break;
		case "Antibiotic":
			script.Disinfect (gameObject);
			break;
		}
	}

}
