﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class BleedingScript : MonoBehaviour {

	private List<Transform> bloodDrops = new List<Transform>();
	private List<Transform> waterGroup = new List<Transform>();

	public bool washed, soaped, rinsed, disinfected, pressured;
	public GameObject startPanel, startPanel1, smallWound, largeWound, faucet, bubblesGroup, waterSpread, ointment, plaster, towelPressure, bandageRolling;
	public Transform hand, hand1, bloodFlow, waterDrop, bloodPrefab, waterPrefab, waterPrefab1;
	public Sprite handWSmallCut, handWLargeCut;
	public TextDisplay textDisplay, guide;

	// Use this for initialization
	void Start () {
		Sounds.Create ();
		startPanel.SetActive (true);
		guide.SetText ("Drag the faucet to the hand to wash the wound.");
		foreach (Transform bloodDrop in bloodFlow) {
			bloodDrops.Add (bloodDrop);
		}
		InvokeRepeating ("DropBlood", 0, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
		if (waterGroup.Count > 0) {
			waterGroup.RemoveAll (item => item == null);
			Bounds bounds = hand.GetComponent<Collider2D> ().bounds;
			foreach (Transform water in waterGroup) {
				Bounds bounds1 = water.GetComponent<Collider2D> ().bounds;
				if (bounds.Intersects(bounds1)) {
					Destroy (water.gameObject);
					if (!waterSpread.activeSelf)
						waterSpread.SetActive (true);
					if (!IsInvoking ("DropWater1"))
						InvokeRepeating ("DropWater1", 0, 0.3f);
					if (soaped) {
						foreach (Transform bubbles in bubblesGroup.transform) {
							bubbles.GetComponent<Rigidbody2D> ().isKinematic = false;
						}
					}
				}
			}
			if (waterGroup.Count == 0) {
				if (waterSpread.activeSelf)
					waterSpread.SetActive (false);
				CancelInvoke ("DropWater1");
				if (!soaped) {
					hand.GetComponent<Image> ().sprite = handWSmallCut;
					washed = true;
					guide.SetText ("Drag the soap to the hand.");
				} else {
					rinsed = true;
					guide.SetText ("Drag the antibiotic to the wound.");
				}
			}
		}

	}

	public void HideStartPanel () {
		startPanel.SetActive (false);
	}

	public void DropBlood() {
		Transform bloodDrop = bloodDrops [Random.Range (0, bloodDrops.Count)];
		Transform blood = Instantiate (bloodPrefab);
		blood.SetParent (bloodDrop, false);
	}

	public void RunFaucet(GameObject draggable) {
		if (!washed || (washed && soaped)) {
			guide.Hide ();
			faucet.SetActive (true);
			InvokeRepeating ("DropWater", 0, 0.04f);
			Invoke ("StopFaucet", 3);
			Sounds.Play (Sounds.faucet);
			if ((washed && soaped)) {
				draggable.SetActive (false);
			}
		} else if (washed && !soaped) {
			textDisplay.Print ("You already washed the wound!");
		} else if (rinsed) {
			textDisplay.Print ("You already rinsed the wound!");
		}
	}

	public void DropWater() {
		Transform water = Instantiate (waterPrefab);
		water.SetParent (hand, false);
		water.position = waterDrop.position;
		waterGroup.Add (water);
	}

	public void DropWater1() {
		foreach (Transform waterDrop1 in waterSpread.transform) {
			Transform water = Instantiate (waterPrefab1);
			water.SetParent (hand, false);
			water.position = waterDrop1.position;
		}
	}

	public void StopFaucet() {
		faucet.SetActive (false);
		CancelInvoke ("DropWater");
		Sounds.Stop ();
	}

	public void ApplySoap(GameObject draggable) {
		if (washed && !soaped && !rinsed) {
			draggable.SetActive (false);
			foreach (Transform bubbles in bubblesGroup.transform) {
				bubbles.GetComponent<Image> ().enabled = true;
			}
			Sounds.Play (Sounds.soap);
			soaped = true;
			guide.SetText ("Drag the faucet again to rinse the wound.");
		} else if (soaped) {
			textDisplay.Print ("You already soaped the hand!");
		} else {
			ShowErrors ();
		}
	}

	public void Disinfect(GameObject draggable) {
		if (rinsed && !disinfected) {
			draggable.SetActive (false);
			ointment.SetActive (true);
			Sounds.Play (Sounds.soap);
			disinfected = true;
			guide.SetText ("Apply the plaster to the wound.");
		} else {
			ShowErrors ();
		}
	}

	public void ApplyPlaster(GameObject draggable) {
		if (disinfected) {
			draggable.SetActive (false);
			plaster.SetActive (true);
			Invoke ("ShowStartPanel", 2);
			Sounds.Play (Sounds.sticker);
		} else {
			ShowErrors ();
		}
	}

	public void ApplyPressure(GameObject draggable) {
		draggable.SetActive (false);
		towelPressure.SetActive (true);
		bloodFlow.gameObject.SetActive (false);
		hand1.GetComponent<Image> ().sprite = handWLargeCut;
		CancelInvoke ("DropBlood");
		Sounds.Play (Sounds.clothFlip);
		pressured = true;
		guide.SetText ("Apply bandage to the wound.");
	}

	public void ApplyBandage(GameObject draggable) {
		draggable.SetActive (false);
		if (pressured) {
			guide.SetText ("Drag down the bandage to wrap the hand.");
			bandageRolling.SetActive (true);
		} else {
			textDisplay.Print ("You should apply pressure to the wound first!");
		}
	}

	public void FinishBandage() {
		bandageRolling.SetActive (false);
		guide.Hide ();
		Invoke ("Finish", 2);
	}

	public void ShowErrors() {
		if (!washed)
			textDisplay.Print("You should wash the wound first!");
		else if (!soaped)
			textDisplay.Print("You should soap the hand first!");
		else if (!rinsed)
			textDisplay.Print("You should rinse the wound first!");
		else if (!disinfected)
			textDisplay.Print("You should apply to antibiotic to the wound first!");
	}

	public void ShowStartPanel () {
		startPanel1.SetActive (true);
	}

	public void NextStage () {
		startPanel1.SetActive (false);
		smallWound.SetActive (false);
		largeWound.SetActive (true);
		guide.SetText ("Drag the towel to apply pressure to the wound.");
	}

	public void Finish() {
		FinalScript.LoadFinalScene (0);
	}
}
