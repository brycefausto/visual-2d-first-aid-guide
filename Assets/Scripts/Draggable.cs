﻿using UnityEngine;
using System.Collections;

public class Draggable : MonoBehaviour {

	private bool targetDragged;
	private Vector3 pos, screenPoint, offset;
	private GameObject text;

	public GameObject target;

	// Use this for initialization
	void Start () {
		pos = transform.localPosition;
		text = transform.Find ("Text").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown () {
		text.SetActive (false);
		screenPoint = Camera.main.WorldToScreenPoint (transform.position);
		offset = transform.position - Camera.main.ScreenToWorldPoint (new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
	}

	void OnMouseDrag () {
		Bounds bounds, bounds1;
		bounds = GetComponent<Collider2D> ().bounds;
		bounds1 = target.GetComponent<Collider2D> ().bounds;
		if (bounds.Intersects (bounds1)) {
			if (!targetDragged) {
				targetDragged = true;
				OnDragTarget ();
			}
		} else {
			targetDragged = false;
		}
		Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
		Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
		transform.position = curPosition;
	}

	void OnMouseUp () {
		Bounds bounds, bounds1;
		bounds = GetComponent<Collider2D> ().bounds;
		bounds1 = target.GetComponent<Collider2D> ().bounds;
		if (bounds.Intersects (bounds1)) {
			OnPutTarget ();
		} else {
			OnHoldOff ();
		}
		transform.localPosition = pos;
		text.SetActive (true);

	}

	public virtual void OnPutTarget () {

	}

	public virtual void OnDragTarget () {

	}

	public virtual void OnHoldOff () {

	}
}
