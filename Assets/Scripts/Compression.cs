﻿using UnityEngine;
using System.Collections;

public class Compression : MonoBehaviour {

	private Animator anim;

	public bool animating, holding;
	public UnconsciousScript script;
	public Compression compression;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//For CPR Hands
	void OnMouseDown () {
		compression.OnMouseDown1 ();
	}

	void OnMouseUp () {
		compression.holding = false;
	}

	//For CPR Person
	public void OnMouseDown1 () {
		if (!script.CompressionMaxed ()) {
			holding = true;
			InvokeRepeating ("Trigger", 0, 0.1f);
		}
	}

	public void Trigger() {
		if (!animating && holding && !script.CompressionMaxed()) {
			animating = true;
			anim.SetTrigger ("Trigger");
			Sounds.Play (Sounds.chestThump);
		}
		if (!holding || script.CompressionMaxed())
			CancelInvoke ("Trigger");
	}

	public void Finish () {
		animating = false;
		script.Compress ();
	}

}
