﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class BurnsScript : MonoBehaviour {

	private bool nextStage;
	private float mins = 0, value = 150;
	private List<Transform> waterGroup = new List<Transform>();

	public float maxMins;
	public bool washed, disinfected;
	public GameObject startPanel, finishPanel, appliedPlastic, waterSpread, faucet, timeDialog, faucetItem;
	public Transform arm, itemPanel, waterDrop, waterPrefab, waterPrefab1;
	public Sprite burnedArm, burnedArm1, burnedArm2, burnedArm3, burnedArm4;
	public Text washTime;
	public Slider painSlider;
	public TextDisplay textDisplay, guide;

	// Use this for initialization
	void Start () {
		Sounds.Create ();
		startPanel.SetActive (true);
		painSlider.maxValue = value;
		painSlider.value = value;
		guide.SetText ("Drag the faucet to the hand to wash the burn. Slide it to 10 minutes.");
	}
	
	// Update is called once per frame
	void Update () {
		if (waterGroup.Count > 0) {
			waterGroup.RemoveAll (item => item == null);
			Bounds bounds = arm.GetComponent<BoxCollider2D> ().bounds;
			foreach (Transform water in waterGroup) {
				Bounds bounds1 = water.GetComponent<Collider2D> ().bounds;
				if (bounds.Intersects(bounds1)) {
					Destroy (water.gameObject);
					if (!waterSpread.activeSelf)
						waterSpread.SetActive (true);
					if (!IsInvoking ("DropWater1"))
						InvokeRepeating ("DropWater1", 0, 0.3f);
				}
			}
			if (waterGroup.Count == 0) {
				if (waterSpread.activeSelf)
					waterSpread.SetActive (false);
				CancelInvoke ("DropWater1");
				if (nextStage)
					arm.GetComponent<Image> ().sprite = burnedArm2;
				else
					arm.GetComponent<Image> ().sprite = burnedArm;
				if (value <= 50) {
					washed = true;
				} else {
					textDisplay.Print ("The burn is not washed enough!");
					faucetItem.SetActive (true);
				}
			}
		}
	}

	public void HideStartPanel() {
		startPanel.SetActive (false);
	}

	public void ShowTimeDialog() {
		timeDialog.SetActive (true);
	}

	public void RunFaucet() {
		if (!washed) {
			faucet.SetActive (true);
			InvokeRepeating ("DropWater", 0, 0.04f);
			Sounds.Play (Sounds.faucet);
			faucetItem.SetActive (false);
		} else {
			textDisplay.Print ("You already washed the burn!");
		}
	}

	public void DropWater() {
		Transform water = Instantiate (waterPrefab);
		water.SetParent (arm, false);
		water.position = waterDrop.position;
		waterGroup.Add (water);
		if (mins < maxMins && value > 50) {
			mins += 0.08f;
			value -= 0.8f;
			string minText = "Wash Time: " + Mathf.RoundToInt(mins) + " min";
			if (mins > 1)
				minText += "s";
			washTime.text = minText;
			painSlider.value = value;
		} else {
			mins = 0;
			StopFaucet ();
		}
	}

	public void DropWater1() {
		foreach (Transform waterDrop1 in waterSpread.transform) {
			Transform water = Instantiate (waterPrefab1);
			water.SetParent (arm, false);
			water.position = waterDrop1.position;
		}
	}

	public void StopFaucet() {
		faucet.SetActive (false);
		CancelInvoke ("DropWater");
		Sounds.Stop ();
		if (value <= 50) {
			washed = true;
			guide.SetText ("The burn reduced its swelling and risk of scarring. Apply antibiotic to the burn.");
		}
	}

	public void Disinfect(GameObject draggable) {
		if (washed) {
			draggable.SetActive (false);
			if (nextStage)
				arm.GetComponent<Image> ().sprite = burnedArm3;
			else
				arm.GetComponent<Image> ().sprite = burnedArm1;
			Sounds.Play (Sounds.soap);
			disinfected = true;
			value -= 25;
			painSlider.value = value;
			guide.SetText ("Wrap the burn with plastic.");
		} else {
			textDisplay.Print ("You should wash the burn first!");
		}
	}

	public void WrapPlastic(GameObject draggable) {
		if (washed && disinfected) {
			guide.Hide ();
			draggable.SetActive (false);
			appliedPlastic.SetActive (true);
			Sounds.Play (Sounds.plastic);
			value = 0;
			painSlider.value = value;
			if (nextStage)
				Invoke ("ShowFinishPanel", 2);
			else
				Invoke ("SetNextStage", 2);
		} else {
			textDisplay.Print ("You should wash the burn and apply antibiotic first!");
		}
	}

	public void SetNextStage() {
		nextStage = true;
		washed = false;
		disinfected = false;
		appliedPlastic.SetActive (false);
		startPanel.SetActive (true);
		startPanel.transform.Find ("Text").GetComponent<Text> ().text = "Treat a 2nd degree burn.";
		arm.GetComponent<Image> ().sprite = burnedArm2;
		value = 200;
		painSlider.maxValue = value;
		painSlider.value = value;
		timeDialog.transform.Find ("Slider").GetComponent<Slider> ().maxValue = 15;
		foreach (Transform items in itemPanel) {
			items.gameObject.SetActive (true);
		}
		guide.SetText ("Drag the faucet to the hand to wash the burn. Slide it to 15 minutes.");
	}

	public void ShowFinishPanel() {
		finishPanel.SetActive (true);
	}

	public void Finish() {
		FinalScript.LoadFinalScene (1);
	}
}
