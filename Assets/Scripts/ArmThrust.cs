﻿using UnityEngine;
using System.Collections;

public class ArmThrust : MonoBehaviour {

	private Animator anim;

	public bool animating, holding;
	public ChokingScript script;
	public ArmThrust armThrust;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update () {

	}

	//For Stomach Area
	void OnMouseDown () {
		armThrust.OnMouseDown1 ();
	}

	void OnMouseUp () {
		armThrust.holding = false;
	}

	//For Abdominal Thrust Panel
	public void OnMouseDown1 () {
		if (script.numArmThrusts < 5) {
			holding = true;
			InvokeRepeating ("Trigger", 0, 0.4f);
		}
	}

	public void Trigger() {
		if (!animating && holding && script.numArmThrusts < 5) {
			animating = true;
			anim.SetTrigger ("Trigger");
			Sounds.Play (Sounds.chestThump);
		}
		if (!holding || script.numArmThrusts == 5)
			CancelInvoke ("Trigger");
	}

	public void Finish () {
		animating = false;
		script.IncreaseArmThrust ();
	}

}
