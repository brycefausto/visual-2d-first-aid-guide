﻿using UnityEngine;
using System.Collections;

public class Cleaner : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D (Collision2D coll) {
		string tag = coll.gameObject.tag;
		if (tag == "Blood" || tag == "Water" || tag == "Bubbles") {
			Destroy (coll.gameObject);
		}
	}

}
