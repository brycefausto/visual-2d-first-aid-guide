﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UnconsciousScript : MonoBehaviour {

	private static string text = "No. of Cycles: {0}/{1}";

	private int maxCompressions = 30, maxBreath = 2, maxCycles = 5;

	public int numCompressions, numBreath, numCycles;
	public GameObject cprHands, cprRescueBreath;
	public Selectable chestCompression, rescueBreath;
	public Animator personBreath;
	public Text display;
	public TextDisplay textDisplay, guide;

	// Use this for initialization
	void Start () {
		Sounds.Create ();
		DisplayNumCycles ();
		guide.SetText ("Select Chest Compression to be able to do chest compressions.");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SwitchAction(int i) {
		switch (i) {
		case 0:
			cprHands.SetActive (false);
			cprRescueBreath.SetActive (false);
			textDisplay.Hide ();
			ResetMaxedNum ();
			DisplayNumCycles ();
			break;
		case 1:
			cprHands.SetActive (true);
			cprRescueBreath.SetActive (false);
			cprRescueBreath.GetComponent<RescueBreath> ().animating = false;
			ResetMaxedNum ();
			DisplayNumCompressions ();
			textDisplay.SetText ("Hold the hand to do compressions. \n(2 counts each for simulation only)");
			if (!CompressionMaxed())
				guide.Hide ();
			rescueBreath.pressed = false;
			break;
		case 2:
			cprHands.SetActive (false);
			cprRescueBreath.SetActive (true);
			DisplayBreathTime ();
			textDisplay.SetText ("Hold the top person to do rescue breath.");
			if (CompressionMaxed() && !BreathMaxed())
				guide.Hide ();
			chestCompression.pressed = false;
			break;
		}
	}

	public void DisplayNumCycles () {
		display.text = string.Format (text, numCycles, maxCycles);
	}

	public void DisplayNumCompressions () {
		string text1 = text + "\nChest Compressions: {2}/{3}\n\n";
		display.text = string.Format (text1, numCycles, maxCycles, numCompressions, maxCompressions);
	}

	public void DisplayBreathTime () {
		string text1 = text + "\nRescue Breath: {2}/{3}\n\n";
		display.text = string.Format (text1, numCycles, maxCycles, numBreath, maxBreath);
	}

	public bool CompressionMaxed () {
		return (numCompressions == maxCompressions);
	}

	public bool BreathMaxed () {
		return (numBreath == maxBreath);
	}

	public void Compress() {
		if (!CompressionMaxed()) {
			numCompressions+=2;
			DisplayNumCompressions ();
		}
		if (CompressionMaxed ()) {
			guide.SetText ("Select Rescue Breath to be able to do rescue breath.");
		}
	}

	public void Breath() {
		if (!BreathMaxed()) {
			numBreath++;
			if (BreathMaxed () && CompressionMaxed ()) {
				numCycles++;
				guide.SetText ("Select Chest Compression to be able to do chest compressions.");
			}
			if (numCycles == maxCycles) {
				rescueBreath.Deselect ();
				personBreath.SetTrigger ("Breath");
				Sounds.Play (Sounds.breath);
				Invoke ("Finish", 3.5f);
			}
			DisplayBreathTime ();
		}
	}

	public void ResetMaxedNum() {
		if (CompressionMaxed () && BreathMaxed ()) {
			numCompressions = 0;
			numBreath = 0;
		}
	}

	public void Finish () {
		FinalScript.LoadFinalScene (4);
	}
}
