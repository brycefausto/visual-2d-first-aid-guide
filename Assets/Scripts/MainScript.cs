﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System.Collections;
using SimpleJSON;

public class MainScript : MonoBehaviour {

	public static bool loaded, backed;
	public static int index;
	public static string[] icons = {
		"1374746425_bleeding_test_x2", 
		"1374746459_burns_test_x2", 
		"1374746475_choking_test_x2", 
		"1374742141_distress_shock_grey_x2", 
		"1374746493_unconcious_test_x2" 
	};
	public static string[] titles = {
		"Bleeding", 
		"Burns", 
		"Choking", 
		"Shock", 
		"Unconscious" 
	};
	public static JSONNode N;

	public GameObject loadingPanel, mainPanel, detailsPanel, marquee, heartRate, qaTab, qaPanel;
	public Transform items, modulesObj, guidelinesObj, qaObj, headerPrefab, contentPrefab, subContentPrefab, videoPlayerPrefab, imageItemPrefab;
	public Text detailsTitle;
	public Scrollbar scrollBar, scrollBar1, scrollBar2, scrollBar3;
	public Image image;

	// Use this for initialization
	void Start () {
		if (!loaded) {
			loaded = true;
			loadingPanel.SetActive (true);
			mainPanel.SetActive (false);
			StartCoroutine (StartLoad ());
		}
		TextAsset textFile = Resources.Load<TextAsset> ("TextFiles/JSONText");
		N = JSON.Parse (textFile.text);
		Transform prefab = Resources.Load<Transform> ("Prefabs/ButtonItem");
		for (int i = 0; i < icons.Length; i++) {
			Transform buttonItem = Instantiate (prefab);
			buttonItem.Find ("Image").GetComponent<Image> ().sprite = Resources.Load<Sprite> ("Icons/" + icons [i]);
			buttonItem.Find ("Text").GetComponent<Text> ().text = titles [i];
			int i1 = i;
			buttonItem.GetComponent<Button> ().onClick.AddListener (() => LoadDetails (i1));
			buttonItem.GetComponent<Button> ().onClick.AddListener (() => GetComponent<AudioSource>().Play());
			buttonItem.SetParent (items, false);
		}
		if (backed) {
			backed = false;
			LoadDetails (index);
			scrollBar.value = 0.5f;
		}
	}
	
	// Update is called once per frame
	void Update () {

	}

	private IEnumerator StartLoad () {
		yield return new WaitForSeconds(2);
		loadingPanel.SetActive (false);
		mainPanel.SetActive (true);
		Fade.StartFade ();
	}

	public static void BackToDetails() {
		backed = true;
		Loader.LoadScene ("Main Scene");
	}

	public void LoadDetails (int i) {
		index = i;
		mainPanel.SetActive (false);
		detailsPanel.SetActive (true);
		GameObject[] textItems = GameObject.FindGameObjectsWithTag ("TextItem");
		foreach (GameObject textItem in textItems)
			Destroy (textItem);
		textItems = GameObject.FindGameObjectsWithTag ("VideoPlayer");
		foreach (GameObject textItem in textItems)
			Destroy (textItem);
		var textData = N [i];
		detailsTitle.text = textData ["title"];
		image.sprite = Resources.Load<Sprite> ("Images/" + textData ["title"]);
		var lines = textData ["modules"];
		for (int i1 = 0; i1 < lines.Count; i1++) {
			string[] subLines = lines[i1].Value.Split ('#');
			Transform content = Instantiate (contentPrefab);
			if (subLines.Length == 1) {
				content.GetComponent<Text> ().text = subLines [0];
				content.SetParent (modulesObj, false);
			} else {
				Transform header = Instantiate (headerPrefab);
				header.GetComponent<Text> ().text = subLines [0];
				header.SetParent (modulesObj, false);
				content.GetComponent<Text> ().text = subLines [1];
				content.SetParent (modulesObj, false);
			}
		}
		lines = textData ["guidelines"];
		for (int i1 = 0; i1 < lines.Count; i1++) {
			string[] subLines = lines[i1].Value.Split ('#');
			Transform content = Instantiate (contentPrefab);
			subLines [0] = (i1 + 1) + ". " + subLines [0];
			content.GetComponent<Text> ().text = subLines [0];
			content.SetParent (guidelinesObj, false);
			if (subLines.Length > 1) {
				Transform subContent = Instantiate (subContentPrefab);
				subContent.GetComponent<Text> ().text = subLines [1];
				subContent.SetParent (guidelinesObj, false);
			}
		}
		lines = textData ["q&a"];
		for (int i1 = 0; i1 < lines.Count; i1++) {
			VideoClip clip = Resources.Load<VideoClip> ("Videos/" + textData ["title"] + " Q&A" + (i1+1));
			if (clip != null) {
				Transform videoPlayer = Instantiate (videoPlayerPrefab);
				videoPlayer.GetComponent<MoviePlayer> ().videoToPlay = clip;
				videoPlayer.SetParent (qaObj, false);
			}
			Sprite image = Resources.Load<Sprite> ("Images/" + textData ["title"] + " Q&A" + (i1 + 1));
			if (image != null) {
				Transform imageItem = Instantiate (imageItemPrefab);
				imageItem.GetComponent<Image> ().sprite = image;
				imageItem.SetParent (qaObj, false);
			}
			string[] subLines = lines[i1].Value.Split ('#');
			Transform content = Instantiate (contentPrefab);
			content.GetComponent<Text> ().text = subLines [0];
			content.SetParent (qaObj, false);
			if (subLines.Length > 1) {
				Transform subContent = Instantiate (subContentPrefab);
				subContent.GetComponent<Text> ().text = subLines [1];
				subContent.SetParent (qaObj, false);
			}
		}
	}

	public void Back () {
		index = 0;
		scrollBar.value = 0;
		scrollBar1.value = 1;
		scrollBar2.value = 1;
		scrollBar3.value = 1;
		mainPanel.SetActive (true);
		detailsPanel.SetActive (false);
	}

	public void SwitchTabs(int i) {
		float value = 0;
		switch (i) {
		case 0:
			value = i;
			break;
		case 1:
			if (qaTab.activeSelf)
				value = 0.5f;
			else
				value = i;
			break;
		case 2:
			value = 1;
			break;
		}
		scrollBar.value = value;
	}

	public void StartTutorial () {
		Loader.LoadScene (titles [index] + " Scene");
	}

	public void Exit () {
		Application.Quit ();
	}
}
