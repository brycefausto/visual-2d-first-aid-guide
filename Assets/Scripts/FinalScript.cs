﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class FinalScript : MonoBehaviour {

	public static int index = 0;

	public GameObject image, message;

	// Use this for initialization
	void Start () {
		Sprite icon = Resources.Load<Sprite> ("Icons/" + MainScript.icons [index]);
		image.GetComponent<Image> ().sprite = icon;
		string text = message.GetComponent<Text> ().text;
		string text1 = string.Format (text, MainScript.titles [index]);
		message.GetComponent<Text> ().text = text1;
		index = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void GoToDetails () {
		MainScript.BackToDetails ();
	}

	public void GoToMain () {
		Loader.LoadScene ("Main Scene");
	}

	public static void LoadFinalScene(int i) {
		index = i;
		SceneManager.LoadScene ("Final Scene");
	}
}
