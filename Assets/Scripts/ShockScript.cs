﻿using UnityEngine;
using System.Collections;

public class ShockScript : MonoBehaviour {
	
	public bool laidDown, raisedLegs, bandaged;
	public GameObject person, panel, panel1, bandage, blanket;
	public TextDisplay textDisplay, guide;


	// Use this for initialization
	void Start () {
		Sounds.Create ();
		guide.SetText ("Select Lay Down to lay down the person to bed.");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void LayDown (Selectable selectable) {
		selectable.disabled = true;
		panel.SetActive (false);
		panel1.SetActive (true);
		laidDown = true;
		guide.SetText ("Select Raise Legs to raise the legs of the person.");
	}

	public void RaiseLegs (Selectable selectable) {
		if (laidDown) {
			selectable.disabled = true;
			person.GetComponent<Animator> ().SetTrigger ("Trigger");
			raisedLegs = true;
			guide.SetText ("Apply the bandage to the wound of the person.");
		} else {
			ShowError ();
		}
	}

	public void ApplyBandage (GameObject draggable) {
		if (laidDown && raisedLegs) {
			draggable.SetActive (false);
			bandage.SetActive (true);
			bandaged = true;
			guide.SetText ("Cover the person with blanket.");
			Sounds.Play (Sounds.clothFlip);
		} else {
			ShowError ();
		}
	}

	public void ApplyBlanket (GameObject draggable) {
		if (laidDown && raisedLegs && bandaged) {
			draggable.SetActive (false);
			blanket.SetActive (true);
			Sounds.Play (Sounds.clothFlip);
			Invoke ("Finish", 2);
		} else {
			ShowError ();
		}
	}

	public void ShowError() {
		if (!laidDown)
			textDisplay.Print ("You should lay down the person first!");
		else if (!raisedLegs)
			textDisplay.Print ("You should raise the legs the person first!");
		else if (!bandaged)
			textDisplay.Print ("You should treat the wounds of the person first!");
	}

	public void Finish() {
		FinalScript.LoadFinalScene (3);
	}

}
