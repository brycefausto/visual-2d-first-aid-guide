﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextDisplay : MonoBehaviour
{

	private static string defMsg = "Message";

	// Use this for initialization
	void Start () {
		
	}

	public void SetText (string msg)
	{
		gameObject.SetActive (true);
		gameObject.GetComponent<Text> ().text = msg;
	}

	public void Print (string msg)
	{
		if (IsInvoking ("Hide")) {
			CancelInvoke ("Hide");
		}
		SetText (msg);
		Invoke ("Hide", 3);
	}

	public void Hide ()
	{
		SetText (defMsg);
		gameObject.SetActive (false);
	}

}
