﻿using UnityEngine;
using System.Collections;

public class UnconsciousSelectable : Selectable {

	public UnconsciousScript script;

	public override void OnSelect() {
		switch (gameObject.name) {
		case "Chest Compression":
			script.SwitchAction (1);
			break;
		case "Rescue Breath":
			script.SwitchAction (2);
			break;
		}
	}

	public override void OnDeselect() {
		script.SwitchAction (0);
	}

}
