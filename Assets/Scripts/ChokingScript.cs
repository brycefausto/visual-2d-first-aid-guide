﻿using UnityEngine;
using System.Collections;

public class ChokingScript : MonoBehaviour
{

	public int numBackHit, numArmThrusts;
	public GameObject chokingPanel, backHitPanel, abdominalThrustPanel, chokeBall;
	public Selectable backHit, abdominalThrust;
	public TextDisplay textDisplay, guide;

	// Use this for initialization
	void Start ()
	{
		Sounds.Create ();
		guide.SetText ("Select Back Hit to be able to do back hits.");
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (chokeBall.activeSelf && chokeBall.transform.localPosition.y <= -350) {
			chokeBall.SetActive (false);
		}
	}

	public void SwitchPanel (int i)
	{
		switch (i) {
		case 0:
			chokingPanel.SetActive (true);
			backHitPanel.SetActive (false);
			abdominalThrustPanel.SetActive (false);
			backHit.pressed = false;
			abdominalThrust.pressed = false;
			ResetArmThrust ();
			ResetBackHit ();
			break;
		case 1:
			chokingPanel.SetActive (false);
			backHitPanel.SetActive (true);
			abdominalThrustPanel.SetActive (false);
			abdominalThrust.pressed = false;
			ResetArmThrust ();
			if (numBackHit < 5)
				guide.SetText ("Do back hit 5 times.");
			break;
		case 2:
			chokingPanel.SetActive (false);
			backHitPanel.SetActive (false);
			abdominalThrustPanel.SetActive (true);
			backHit.pressed = false;
			ResetBackHit ();
			if (numBackHit == 5 && numArmThrusts < 5)
				guide.SetText ("Do abdominal thrusts 5 times.");
			break;
		}
	}

	public void ResetBackHit ()
	{
		if (numBackHit > 0 && numBackHit < 5) {
			numBackHit = 0;
			textDisplay.Print ("You should hit the back of the person 5 times!");
		}
	}

	public void ResetArmThrust ()
	{
		if (numArmThrusts > 0 && numArmThrusts < 5) {
			numArmThrusts = 0;
			textDisplay.Print ("You should do abdominal thrusts to the person 5 times!");
		}
	}

	public void IncreaseBackHit ()
	{
		if (numBackHit < 5) {
			numBackHit++;
		}
		if (numBackHit == 5) {
			guide.SetText ("Select Abdominal Thrust to be able to do abdominal thrusts.");
		}
	}

	public void IncreaseArmThrust ()
	{
		if (numArmThrusts < 5) {
			numArmThrusts++;
		}
		if (numBackHit == 5 && numArmThrusts == 5) {
			backHit.disabled = true;
			abdominalThrust.disabled = true;
			chokeBall.SetActive (true);
			guide.Hide ();
			Invoke ("Finish", 3);
		}
	}

	public void Finish ()
	{
		FinalScript.LoadFinalScene (2);
	}

}
