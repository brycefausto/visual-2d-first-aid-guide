﻿using UnityEngine;
using System.Collections;

public class Sounds : MonoBehaviour {

	private static Sounds instance;
	private static AudioSource source;

	private static bool loaded;
	public static AudioClip breath, buttonClick, chestThump, clothFlip, handHit, maskBreath, plastic, faucet, soap, sticker, success;

	// Use this for initialization
	void Start () {
		if (!loaded) {
			loaded = true;
			breath = LoadSound ("breath");
			buttonClick = LoadSound ("button_click");
			chestThump = LoadSound ("chest_thump");
			clothFlip = LoadSound ("cloth_flip");
			handHit = LoadSound ("hand_hit");
			maskBreath = LoadSound ("mask_breath");
			plastic = LoadSound ("plastic");
			faucet = LoadSound ("faucet");
			soap = LoadSound ("soap");
			sticker = LoadSound ("sticker");
			success = LoadSound ("success");
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (source != null) {
			if (!source.isPlaying)
				source.clip = null;
		}
	}

	public static void Create () {
		if (instance == null) {
			instance = new GameObject ("Sounds").AddComponent<Sounds> ();
			source = instance.gameObject.AddComponent<AudioSource> ();
			source.playOnAwake = false;
		}
	}

	public AudioClip LoadSound(string soundName) {
		return Resources.Load<AudioClip> ("Audio/" + soundName);
	}

	public static void Play (AudioClip clip) {
		source.clip = clip;
		source.Play ();
	}

	public static void PlayLoop(AudioClip clip) {
		source.loop = true;
		Play (clip);
	}

	public static void Stop() {
		source.Stop ();
		source.clip = null;
	}
}
