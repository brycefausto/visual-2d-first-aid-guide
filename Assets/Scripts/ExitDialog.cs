﻿using UnityEngine;
using System.Collections;

public class ExitDialog : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Yes() {
		MainScript.BackToDetails ();
	}

	public void No() {
		gameObject.SetActive (false);
	}

	public void Show () {
		gameObject.SetActive (true);
	}
}
