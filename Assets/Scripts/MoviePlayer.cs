﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;


public class MoviePlayer : MonoBehaviour
{

	private VideoPlayer videoPlayer;
	private VideoSource videoSource;
	private AudioSource audioSource;
	private RawImage image;
	private GameObject playIcon;

	public VideoClip videoToPlay;
	
	// Use this for initialization
	void Start ()
	{
		image = GetComponent<RawImage> ();
		//Add VideoPlayer to the GameObject
		videoPlayer = GetComponent<VideoPlayer> ();

		playIcon = transform.Find ("PlayIcon").gameObject;
		StartCoroutine (PlayVideo ());
	}

	IEnumerator PlayVideo ()
	{
		//Disable Play on Awake for both Video
		videoPlayer.playOnAwake = false;

		//We want to play from video clip not from url
        
		videoPlayer.source = VideoSource.VideoClip;


		//Set video To Play then prepare Audio to prevent Buffering
		videoPlayer.clip = videoToPlay;
		videoPlayer.Prepare ();

		//Wait until video is prepared
		WaitForSeconds waitTime = new WaitForSeconds (1);
		while (!videoPlayer.isPrepared) {
			//Prepare/Wait for 5 sceonds only
			yield return waitTime;
			//Break out of the while loop after 5 seconds wait
			break;
		}

		videoPlayer.Play ();
		image.texture = videoPlayer.texture;
		videoPlayer.Stop ();

	}

	IEnumerator ProcessVideo () {
		while (videoPlayer.isPlaying) {
			yield return null;
		}
		videoPlayer.Play ();
		yield return new WaitForSeconds (0.1f);
		image.texture = videoPlayer.texture;
		videoPlayer.Stop ();
		playIcon.SetActive (true);
	}

	// Update is called once per frame
	void Update ()
	{
	}

	public void Play ()
	{
		if (!videoPlayer.isPlaying) {
			videoPlayer.Play ();
			playIcon.SetActive (false);
			StartCoroutine (ProcessVideo ());
		}
	}

	public void Stop ()
	{
		videoPlayer.Stop ();
	}
	
}
