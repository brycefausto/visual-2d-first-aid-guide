﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Selectable : MonoBehaviour {

	private Color32 defaultColor, pressedColor, disabledColor;
	private Image image;

	public bool pressed, disabled;

	// Use this for initialization
	void Start () {
		image = GetComponent<Image> ();
		defaultColor = new Color32 (255, 255, 255, 255);
		pressedColor = new Color32 (240, 240, 240, 255);
		disabledColor = new Color32 (150, 150, 150, 255);
	}

	// Update is called once per frame
	void Update () {
		if (disabled)
			image.color = disabledColor;
		else if (pressed)
			image.color = pressedColor;
		else
			image.color = defaultColor;
	}

	void OnMouseDown () {
		if (!disabled) {
			pressed = !pressed;
			if (pressed)
				OnSelect ();
			else
				OnDeselect ();
		}
	}

	public void Deselect() {
		pressed = false;
		OnDeselect ();
	}

	public virtual void OnSelect() {

	}

	public virtual void OnDeselect() {

	}
}
