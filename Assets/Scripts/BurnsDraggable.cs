﻿using UnityEngine;
using System.Collections;

public class BurnsDraggable : Draggable {

	public BurnsScript script;

	public override void OnPutTarget () {
		switch (gameObject.name) {
		case "Faucet":
			script.ShowTimeDialog ();
			break;
		case "Antibiotic":
			script.Disinfect (gameObject);
			break;
		case "Plastic Wrapper":
			script.WrapPlastic (gameObject);
			break;
		}
	}

}
